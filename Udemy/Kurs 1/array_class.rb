#Array
=begin
student_names = ["John", "David", "Henry", "Bob", "Victor"]
student_age = [29, 31, 35, 43, 53]

puts student_names[0]
puts student_names[2]
puts student_names[4]

puts student_age[0]
puts student_age[3]
puts student_age[4]
=end

#Create en empty array
#[]
#student_array = Array.new # creates a new array

=begin
#Fill the array with elements
student_array[0] = "John"
student_array[1] = "Bob"
student_array[2] = "Julie"
student_array[3] = "Lisa"

puts student_array
print student_array
puts student_array.class
puts student_array.length # counts the length of the array - counts the elements inside the array
puts student_array.empty?
puts student_array.sort
=end

#create en empty array with defined number of elements
student_age = Array.new(5)
print student_age
student_age[0] = 30
student_age[1] = 40
student_age[2] = 120
student_age[3] = 5
student_age[4] = 20
print student_age

student_age[5] = 11
student_age[6] = 12345
print student_age