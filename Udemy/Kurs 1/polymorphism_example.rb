# Polymorphism - same method with different forms of interpretation

class Vehicle
    def initialize(make, model, year)
        @make = make
        @model = model
        @year = year
    end

    def start
        puts "Vehice is starting"
    end

    def stop
        puts "Vehicle is stopping"
    end
end

class SUV < Vehicle
    def start
        puts "SUV Vehicle (#{@make}, #{@model} and #{@year}) is starting"
    end

    def stop
        puts "SUV Vehicle (#{@make}, #{@model} and #{@year}) is stopping"
    end
end

class Semi < Vehicle
    def start
        puts "Semi Vehicle (#{@make}, #{@model} and #{@year}) is starting"
    end

    def stop
        puts "Semi Vehicle (#{@make}, #{@model} and #{@year}) is stopping"
    end
end

class Motorbike < Vehicle
    def start
        puts "Motorbike (#{@make}, #{@model} and #{@year}) is starting"
    end

    def stop
        puts "Motorbike (#{@make}, #{@model} and #{@year}) is stopping"
    end
end

subaru = SUV.new("Subaru", "XV", "2016")
subaru.start
subaru.stop
puts "----------"
volvo = Semi.new("Volvo", "VNL", "2010")
volvo.start
volvo.stop
puts "----------"
harley = Motorbike.new("Harley", "kA", "1995")
harley.start
harley.stop
puts "----------"
käfer = Vehicle.new("VW", "Käfer", "1950")
käfer.start
käfer.stop
