#Class - Blueprint from which objects can be created.
#Class - Contains methods, variables and so on.
#Class - Classnames need to be uppercase!
class Pet
    def cute
        puts "I am cute"
    end
end

class Dog < Pet # Subclasszuordnung über Dog < Pet, wobei Pet die übergeordnete class ist.
    def name_of_dog(name)
        puts "I am #{name}"
    end

    def bark
        puts "I am barking"
    end
end

#create an object from the class.
#Classname.new

#create an object called corgi with the methods of the class "dog"
#corgi = Dog.new
# corgi.name_of_dog("Jack")
# corgi.bark


corgi = Dog.new
corgi.cute
