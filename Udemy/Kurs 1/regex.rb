#Regular expressions

#/^/ - Beginning of string
#/$/ - End of string
#/.*/ - Any character of 0 or more occurences (Punkt für jeden Charakter, Stern für 0 oder mehr)
#/word/ = any word matching

word1 = 'Automobile'
word2 = 'Automechanic'
word3 = 'MechanicAuto'
word4 = 'AutoasdfasdfasdfMechanic'
word5 = 'Autoasdfasdfmobile'
word6 = 'automobile'

def reg_match_with_word(string1, string2)
    if string1 =~ /#{string2}/
        puts "String matches"
    else 
        puts "No match"
    end
end

reg_match_with_word(word1, word2)
reg_match_with_word(word1, word6.capitalize)
puts "-----------------"
def reg_match_beginning_word(string1, regExPattern)
    if string1 =~ /^#{regExPattern}/
        puts "#{string1} begins with #{regExPattern}"
    else
        puts "No match"
    end
end 

reg_match_beginning_word(word1, "Auto")
reg_match_beginning_word(word2, "Auto")
reg_match_beginning_word(word3, "Auto")

puts "------------------"

def reg_match_ends_with_word(string1, regExPattern)
    if string1 =~ /#{regExPattern}$/
        puts "#{string1} ends with #{regExPattern}"
    else
        puts "No match"
    end
end 

reg_match_ends_with_word(word1, "Auto")
reg_match_ends_with_word(word2, "Auto")
reg_match_ends_with_word(word3, "Auto")

puts "------------------"

def reg_match_starts_with_and_ends_with_word(string1, regExPattern1, regExPattern2)
    if string1 =~ /^#{regExPattern1}.*#{regExPattern2}$/
        puts "#{string1} starts with #{regExPattern1} and ends with #{regExPattern2}"
    else
        puts "No match"
    end
end 

reg_match_starts_with_and_ends_with_word(word1.capitalize, "Auto", "Mechanic".downcase)
reg_match_starts_with_and_ends_with_word(word2.capitalize, "Auto", "Mechanic".downcase)
reg_match_starts_with_and_ends_with_word(word3.capitalize, "Auto", "Mechanic".downcase)
reg_match_starts_with_and_ends_with_word(word4.capitalize, "Auto", "Mechanic".downcase)
reg_match_starts_with_and_ends_with_word(word5.capitalize, "Auto", "Mechanic".downcase)
reg_match_starts_with_and_ends_with_word(word6.capitalize, "Auto", "Mechanic".downcase)