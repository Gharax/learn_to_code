#Inheritance- forming a new class from an existing base class or parent class
#base/parent/super class 
#child class/derived class    

#Parent/base/super class
# class Animal
#     def breath
#         puts "inhale and exhale"
#     end
# end

# #childclass

# class Dog < Animal
#     def bark
#         puts "bark bark"
#     end
# end

# #create an object
# d1 = Dog.new
# d1.breath
# d1.bark

#Parent/base/super class
class Box
    def initialize(w,h)
            @width = w
            @height = h
    end

    def display_box_name
        puts "I am box class"
    end
end

#child class
class Smallbox < Box
    def print_area
        @area = @width * @height
        puts "Area of the small box is: #{@area}"
    end

    def display_price
        puts "Price is 1200€"
    end
end

#child class
class Bigbox < Box
    def initialize(v, w, h)
        @volume = v
        @width = w
        @height = h
    end

    def print_area
        @area = @volume * @width * @height 
        puts "The printarea is #{@area}"
    end
end

#create an object from parent class
b1 = Box.new(12,15)
b1.display_box_name
puts "---------"
#create an object from child class Smallbox
b2 = Smallbox.new(12,15)
b2.display_box_name
b2.display_price
puts "---------"
#create an object from child class Bigbox
b3 = Bigbox.new(10,12,15)
b3.display_box_name
b3.print_area