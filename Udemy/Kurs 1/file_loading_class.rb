#load - reads and parses (analysiert) file every time you call 'load'
#require - reads and parses files only once. Generally used to import ruby libraries and also needs absolute paht of the file. (needs full directorypath)
#require_related - can be used instead of 'require' if they are in the same directory (doesn't need full directorypath)

#require files

#load - read and parse file every time you call load
# load 'C:/Users/Philipp/Desktop/Projekte/Übungen/Udemy/types_of_variables_class.rb'

# file1 = Algebra.new
# file1.value

#require - read and parse file only once
# require 'C:/Users/Philipp/Desktop/Projekte/Übungen/Udemy/types_of_variables_class.rb'

# file1 = Algebra.new
# file1.value

require 'Time'                      # Doesn't need a path if we require standard ruby libraries.
c1 = Time.new
puts c1

#require_relative - 
# require_relative 'types_of_variables_class.' # doesn't need the complete path and no .rb-ending if it is in the same folder.

# file1 = Algebra.new
# file1.value
