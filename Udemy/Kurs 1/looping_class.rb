#for i in 1..10
#   puts "Hello world -#{i}"
#end

#------------------------------------------
#For Loop (steigende Zahl bis Grenze ÜBERSCHRITTEN ist)
=begin
x = 0
number = 10 

for x in 0..number
    puts "number is: #{x}"
    x = x+1
end
=end

#------------------------------------------
#While Loop (sinkende Zahl bis Ende erreicht ist)
=begin
x = 10
number = 0

while x >= number do
    puts "number is: #{x}"
    x -= 1
end
=end

#------------------------------------------
#Until Loop (steigende Zahl bis Grenze ERREICHT ist)
=begin
x = 0
number = 10

until x >= number do
    puts "Number is: #{x}"
    x += 1
end
=end

#---------------------------------------------
#for loop - to find even numbers from 1..10
=begin
for i in 1..10
    if i % 2 == 0
        puts "#{i} is an even number."
    else 
        puts "#{i} is an odd number."
    end
end
=end

#------------------------------------------------
=begin
city_array = ["New York", "New Jersey", "Washington D.C.", "Chicago", "Los Angeles"]

for i in city_array
    puts "City: #{i}"
end
=end
#----------------------------------------------

#Iteration
#times - only can be used in integers
=begin
5.times {puts "hello ruby"}
puts "------"
5.times do puts "hello ruby" end
=end
#-------------------------------------------

#each loop - used primarily in array and hashes

#counties_array = ["polk", "fairfax", "orange", "dustin"]

#for county in counties_array
#   puts "county: #{county}"
#end

#alternativ dazu:

#counties_array.each do |county|
#    puts "county: #{county}"
#end

alphabet = ["a", "b", "c", "d", "e"]

alphabet.each {|letter|
puts "letter: #{letter}"
}
