# puts File.exist?('students.rb')
# puts File.exist?('\Users\Philipp\Desktop\Projekte\Übungen\Udemy\array_class.rb')

#Write contents in a file
# aFile = File.new('ruby.txt', 'w')                         # w = write
# aFile.syswrite('I have mastered the basics of ruby now')
# aFile.close

#REading the contents of a file
# aFile = File.new('ruby.txt', 'r')                           # r = read
# aFile.each {|line| puts line}
# aFile.close

#exception handling - throw an error if no file found
#begin-rescue - keyword for exception handling
def read_my_file(file)
    begin
        aFile = File.new(file, 'r')                        
        aFile.each {|line| puts line}
        aFile.close
    rescue
        raise ("File not found, idiot!")                    #instead of raise we could use puts, which would result in a simple line instead of a failuremessage. Only raise stops the execution.
    end
end

read_my_file("ruby.txt")
puts "----------"
read_my_file("test.txt")
