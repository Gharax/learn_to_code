#Modules - Way of grouping together classes, methods and constants
#Modules - Alternative way of doing multiple inheritances

# module Animal
#     class Dog
#         def bark
#             puts "I am barking"
#         end
    
#         def sleep
#             puts "I am sleeping"
#         end

#         def eat
#             puts "I am eating"
#         end
#     end
# end

# #Create an object

# a1 = Animal::Dog.new # Since class Dog is inside module Animal, we need to address it with Animal::Dog
# a1.bark
# a1.sleep
# a1.eat
#----------------------------------------------------
# module Bigsample
#     class SampleA
#         def SampleA
#             puts "Sample A"
#         end
#     end

#     class SampleB
#         def SampleB
#             puts "Sample B"
#         end
#     end
# end

# a1 = Bigsample::SampleA.new
# a1.SampleA

# a2 = Bigsample::SampleB.new
# a2.SampleB
#------------------------------------------------------------

#Multiple inheritance - The method needs to be in a module, thats get included in a class, that is inside another module
#So it looks like this: Bigmodule -> class -> smallmoduleA with method A // smallmoduleB with method B // etc.

module Z
    def z1
        puts "I am z1"
    end
end


class Dog
    include Z
    def bark
        puts "I am barking"
    end
end

module A
    def a1
        puts "I am a1"
    end
end

module B
    def b1
        puts "I am b1"
    end
end

module C
    def c1
        puts "I am c1"
    end
end

module D
    def d1
        puts "I am d1"
    end
end

module Bigsample
    class Testsample < Dog
        include A
        include B
        include C
        include D
    end
end

test = Bigsample::Testsample.new
  test.a1
  test.b1
  test.c1
  test.d1
  test.bark                     # calls a method from the superclass
  test.z1                       # calls a method from an included module from the superclass
