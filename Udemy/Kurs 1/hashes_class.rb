# grades = {"john" => 3.8, "julie" => 3.9}
# puts grades["john"]
# puts grades["julie"]

# print grades.keys
# print grades.values

# students = {1 => "Ashley", 2 => "Max", 3 => "Matt", 4 => "Scott", 5 => "Chris"}
# puts students[3]
# print students.keys
# print students.values

#-----------------------------------------
#Find the key using the value - only works with unice keys

# grades = {"john" => 3.8, "julie" => 3.9, "Hannes" => 3.9}
# puts grades.key(3.9)

h = Hash.new
h["brandy"] = 3.9
h["ashton"] = 2.9
h["steve"] = 4.0

# puts h
# print h

h.each {|unfug, mumpitz|
    puts "#{unfug}'s ssn is: #{mumpitz}"
}
