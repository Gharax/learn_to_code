# def add(num1,num2)
#     total = num1 + num2
#     return total
# end

# puts add(1,2)
# puts add(4,5)
# puts add(12,12)

# def greet(name)
#     puts "Hello #{name}, welcome to hell" # Durch die Nutzung von puts in der definierten Methode ist es später nicht nötig.
# end
# greet("Becky")
# greet("Benny")
# greet("Sabi")
# greet("Alledreizusammen")

#--------------------------------------

#Method without default value
# def prog_language(p1, p2)
#     puts "The programming language 1 is: #{p1}"
#     puts "The programming language 2 is: #{p2}"
# end

# prog_language("Java", "Ruby")
# prog_language("Ruby")

#Method with default value - the default value can be overwritten, if it's not it will show the default value

def prog_language(p1="C++", p2)
    puts "The programming language 1 is: #{p1}"
    puts "The programming language 2 is: #{p2}"
end

prog_language("Java", "Ruby")
prog_language("Ruby")