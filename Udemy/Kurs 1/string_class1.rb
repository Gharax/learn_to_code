#class1

name = "John Doe"
puts name
puts name.class

address = '7843 California Avenue, New York, NY10021'
puts address
puts address.class

=begin
mit # kommentieren wir Zeilen aus, der Code ignoriert sie.
Alternativ nutzen wir begin und end um ganze Blöcke zu kommentieren.
=end

city = 'New York City'
print city

state = "New York"
print state

#Unterschied puts zu print: 
#puts gibt den Inhalt in einer neuen Zeile aus
#print gibt den Inhalt in der aktuellen Zeile aus

#Einzelne Anführungszeichen >'< brauchen weniger Rechenleistung als doppelte Anführungszeichen >"<
# >"< wird lediglich für String interpolation (?????) benötigt.