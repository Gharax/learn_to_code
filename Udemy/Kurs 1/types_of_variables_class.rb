#Types of variables

#Instance variables - Variable that are used by instance methods. Instance variables always start with @

# class Customer
#     def initialize(id, name, address)
#         #create instance variables
#         @cust_id = id
#         @cust_name = name
#         @cust_addr = address
#     end

#     def display_cust_details
#         puts "Customer ID is #{@cust_id}"
#         puts "Customer name is #{@cust_name}"
#         puts "Customer address is #{@cust_addr}"
#     end
# end

# c1 = Customer.new(729,"john", "Wunschadresse")
# c1.display_cust_details

# c2 = Customer.new(824, "Maria","Musterort")
# c2.display_cust_details

#Class variables - Variables that are used by the class and can be shared among the descendants
#Class variables start with @@

# class Customer
#     @@no_of_customers = 0
#     def initialize(id, name, address)
#         #create instance variables
#         @cust_id = id
#         @cust_name = name
#         @cust_addr = address
#         @@no_of_customers += 1
#     end

#     def display_cust_details
#         puts "Customer ID is #{@cust_id}"
#         puts "Customer name is #{@cust_name}"
#         puts "Customer address is #{@cust_addr}"
#     end

#     def count_no_of_customers
#         puts "Total number of customers is #{@@no_of_customers}"
#     end

# end

# c1 = Customer.new(729,"john", "Wunschadresse")
# c1.display_cust_details
# c1.count_no_of_customers

# c2 = Customer.new(824, "Maria","Musterort")
# c2.display_cust_details
# c2.count_no_of_customers

#Global variables - variables that can be used throughout the program. It always starts with $

# $age = 29
# class C1
#     def print_global_c1
#         puts "Global variable is #{$age}"
#     end

#     def self.print_global_again                     # self ersetzt hier den Classnamen C1
#         puts "Global variable again is #{$age}"
#     end
# end

# puts $age                   #druckt den global verfügbaren Wert von age, möglich durch $
# C1.print_global_again       #aktiviert die Methode self.print_global_again der Class C1

# c1 = C1.new                 #erschafft ein Objekt "c1" der Class "C1"
# c1.print_global_c1          #aktiviert die Methode print_global_c1

#Local variables - Variables that are used in the method and normally hard coded
# class Car
#     def type
#         type = "Toyota"
#         puts type
#     end
# end

# c1 = Car.new
# c1.type

#Das negative an local variables ist, dass sie jedes mal im Code geändert werden müssen und nicht über Iterationen (#{xyz}) geändert werden können.

#CONSTANTS - Variables that does not change. Always written in upper case.


class Algebra
    PI = 3.1415
        def value
            puts "Value is #{PI}"
        end
end
# c1 = Algebra.new
# c1.value
# puts c1.PI