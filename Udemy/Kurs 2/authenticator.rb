users =  [
            {username: "mashrur", password: "password1"},
            {username: "jack", password: "password2"},
            {username: "arya", password: "password3"},
            {username: "jonsnow", password: "password4"},
            {username: "heisenberg", password: "password5"},
            {username: "123", password: "456"},
]

tries = 1

puts 'Welcome to the authenticator'
25.times {print '-'}
puts
puts 'This programm will take input from the user and compare the password.'
puts 'If the password is correct, you will get back the user object.'

while tries <= 3 do
  print 'Username: ' 
  login_name = gets.chomp.to_s
  print 'Password: ' 
  login_password = gets.chomp.to_s
    users.each do |member|
      if member[:username] == login_name && member[:password] == login_password
        puts "{:username => #{member[:username]}, :password => #{member[:password]}}"
      else
        puts 'Credentials were not correct.'
      end
    end
  tries = tries + 1    
end