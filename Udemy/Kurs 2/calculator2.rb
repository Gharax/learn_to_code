def multiply(num_1, num_2)
  puts "num_1 multiplied with num_2 is #{num_1 * num_2}."
end

def divide(num_1, num_2)
  puts "num_1 divided through num_2 is #{num_1 / num_2}."
  puts "The mod is #{num_1 % num_2}."
end

def add(num_1, num_2)
  puts "num_1 plus num_2 is #{num_1 + num_2}."
end

def subtract(num_1, num_2)
  puts "num_1 minus num_2 is #{num_1 - num_2}."
end

puts "Simple calculator"
25.times {print "-"}
puts
puts "What do you want to do?"
puts "Enter 1 for multiplication, 2 for division, 3 for addition or 4 for subtraction."
calc_method = gets.chomp
puts "Please enter the first number."
num_1 = gets.chomp.gsub(",", ".").to_f
puts "Please enter the second number."
num_2 = gets.chomp.gsub(",", ".").to_f
if calc_method == "1"
  multiply(num_1, num_2)
elsif calc_method == "2"
  divide(num_1, num_2)
elsif calc_method == "3" 
  add(num_1, num_2)
elsif calc_method == "4"
  subtract(num_1, num_2)
end