# initialize a repository from existing code
#   move to the future path of the repository
#   git init

# removing the tracking of a repository
#   rm -rf .git // rm for 'remove', -rf as modificator, .git to describe what should be deleted

# before first commit
#   git status // shows the status of the currend repository. for example which files have changed, are ready to commit etc.

# creating files
#   touch <file>

# ignore files permanant
#   touch .gitignore
#   add files in the .gitignore-file, including the .gitignore itself

# add files to staging area
#   git add -A // -A to add all files
#   git add <file> // to add only that file
#   git status // to check if it worked

# remove files from staging area
#   git reset // to remove all files
#   git reset <file> // to remove only that file
#   git status // to check if it worked

# commit files from staging area
#   git commit -m "comment what got changed" // -m is the modificator to add a comment
#   git status // to check if it worked
#   git log // shows the log of the last commit

# pushing changes to the remote repository
#   git pull origin master // pulls potentially changed data from the remote repo, origin is the name of the repo, master is the name of the branch
#   git push origin master // pushs our commits to the remote repo

# cloning a remote repository
#   git clone <url> <path to clone it into> 
#   git clone https://gitlab.com/Gharax/example_repo.git /mnt/c/.../path_of_new_repo // should look like this

# viewing information about the remote repository
#   git remote -v // lists the url from where it fetches and pushes the data from and to
#   git branch -a // lists all branches in the local AND remote repository

# create a branch for a desired feature
#   git branch // shows all actual available local branches
#   git branch -a // shows local AND remote branches
#   git branch calc-divide // creates the new branch, calc-divide would be the branchname
#   git checkout calc-divide // activates the branch to work in it

# merge a branch
#   git checkout master // activate the masterbranch
#   git pull origin master // loads the remote masterbranch to get potential updates
#   git branch --merged // shows branches that got merged until now
#   git merge calc-divide // merges the named branch into the active branch, master in this case
#   git push origin master // pushes the branch into the remote repository

# delete a branch
#   git branch --merged // shows branches that got merged until now, in this case to check if calc-divide got merged
#   git branch -d calc-divide // deletes the branch, -d is the modificator to delete
#   git branch -a // lists all branches, here to see that calc-divide got deleted locally, but is still on the remote repo
#   git push origin --delete calc-divide // sends a delete-order of the branch calc-divide to the remote repo
#   git branch -a // shows that calc-divide got deleted in the remote repo

# -------------------------------------------------------------------------------

# example for a common workflow
#   git branch <branchname> // create branch
#   git checkout <branchname> // use branch
#   git status // check for modified files
#   git add -A // add all modified files to the stagingarea
#   git commit -m "message" // commit files with a note what got changed to the LOCAL branch
#   git push -u origin <branchname> // -u connects the local <branchname> with the remote <branchname>, so that we can use git push and git pull in the future
#   git branch -a

test
